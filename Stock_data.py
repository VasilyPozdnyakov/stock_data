# -*- coding: utf-8 -*-
"""
Created on Wed Oct 28 11:46:49 2020

@author: Pozdnyak
"""
from datetime import datetime
import requests
from bs4 import BeautifulSoup
import pandas as pd 

tickers = ['INTC','MSFT','JPM', 'V','DIS','GE','AXP','SO','TSN', 'PG', 'KO','PEP',
           'CSCO', 'CCL', 'VZ', 'SPCE', 'KHC', 'PFE', 'GILD', 'T', 'BIIB', 
           'BMW.DE','DTE.DE','BAYN.DE',
           'RDS-A','IBM', 'ALB', 'ABBV', 'MOMO', 'MMM'
           ]


#RES = []
#rate=[]

while True:
    PE, FPE, EX_DIV, DIV, TARGET, PRICE = [],[],[],[],[],[]

    for ticker in tickers:
        url = 'https://finance.yahoo.com/quote/'+ticker # url страницы
        html = requests.get(url)
        soup = BeautifulSoup(html.text, 'lxml')
        PRICE.append (float(soup.find('span', attrs={"class": "Trsdu(0.3s) Fw(b) Fz(36px) Mb(-4px) D(ib)"}).string))
        
        if soup.find('td', attrs={"data-test": "PE_RATIO-value"}).string != "N/A":
            PE.append (float(soup.find('td', attrs={"data-test": "PE_RATIO-value"}).string))
        else:
            PE.append (-1.0)
            
        EX_DIV.append (soup.find('td', attrs={"data-test": "EX_DIVIDEND_DATE-value"}).string)
        DIV.append (soup.find('td', attrs={"data-test": "DIVIDEND_AND_YIELD-value"}).string)
        
        if soup.find('td', attrs={"data-test": "ONE_YEAR_TARGET_PRICE-value"}).string != "N/A":
            TARGET.append (float(soup.find('td', attrs={"data-test": "ONE_YEAR_TARGET_PRICE-value"}).string))
        else:
            TARGET.append (-1.0)
        #RES.append (soup.find('td', attrs={"data-test": "DIVIDEND_AND_YIELD-value"}).string)
        
        url_analys2 = 'https://stockanalysis.com/stocks/'+ticker
        html2 = requests.get(url_analys2)
        soup2 = BeautifulSoup(html2.text, 'lxml')
        
        if soup2.find ('div',attrs = {"class":"info"}) != None:
            temp2 = soup2.find ('div',attrs = {"class":"info"}).find_all('tr')
            temp3=temp2[6].text[12:16]
            if "n/a" not in temp3:
                FPE.append ("{0:.3f}".format(float(temp3)))
            else:
                FPE.append (-1.0)
        else:
            url_analys = 'https://finance.yahoo.com/quote/'+ticker+'/key-statistics'
            html1 = requests.get(url_analys)
            soup1 = BeautifulSoup(html1.text, 'lxml')
            temp = soup1.find_all('td',
                                  attrs = {"class" : "Ta(c) Pstart(10px) Miw(60px) Miw(80px)--pnclg Bgc($lv1BgColor) fi-row:h_Bgc($hoverBgColor)"})
            if len(temp)>3 and (temp[3].string != "N/A"):
                FPE.append("{0:.3f}".format(float(temp[3].string)))
            else:
                if len(temp)<4:
                    FPE.append (0.0)
                else:
                    FPE.append (-1.0)
            
    Differ = ["{0:.3f}".format((x - y)/y)  for (x, y) in zip(TARGET[:], PRICE[:])]
    div_value=[]
    div_percent=[]
    for i in range (len(DIV)):
        div_value.append(DIV[i][0:4])
        div_percent.append(DIV[i][-6:-1])
        
    full = list(zip(tickers,PRICE,TARGET,Differ,PE,FPE,EX_DIV,div_value,div_percent))
    
    full_pd = pd.DataFrame(full)
    full_pd.columns = ['Name','Price','Target',
                       'Upside','PE','Forward_PE','Ex_div',
                       'Div_value','Div_percent']
    
    sort_data = full_pd.sort_values(by=['PE'], ascending=True)
    
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    print ("Last update at "+str(current_time))
    
    sort_data.to_csv("stocks.csv", index=None)










